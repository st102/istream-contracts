
#include "card.hpp"

std::ostream& operator<<(std::ostream& os, Rank r)
{
    switch(r)
    {
        case Ace:
            return os << "A";
        case Two:
            return os << "2";
        case Three:
            return os << "3";
        case Four:
            return os << "4";
        case Five:
            return os << "5";
        case Six:
            return os << "6";
        case Seven:
            return os << "7";
        case Eight:
            return os << "8";
        case Nine:
            return os << "9";
        case Ten:
            return os << "T";
        case Jack:
            return os << "J";
        case Queen:
            return os << "Q";
        case King:
            return os << "K";
    }
}

std::ostream& operator<<(std::ostream& os, Suit s)
{
    switch(s)
    {
        case Spades:
            return os << "S";
        case Diamonds:
            return os << "D";
        case Hearts:
            return os << "H";
        case Clubs:
            return os << "C";
    }
}

bool operator==(Card a, Card b)
{
    return a.get_rank() == b.get_rank() && a.get_suit() == b.get_suit();
}

bool operator!=(Card a, Card b)
{
    return !(a == b);
}

bool operator<(Card a, Card b)
{
    if (a.get_suit() < b.get_suit())
        return true;
    if (b.get_suit() < a.get_suit())
        return false;
    return a.get_rank() < b.get_rank();
}

bool operator>(Card a, Card b)
{
    return !(a<b);
}

bool operator<=(Card a, Card b)
{
    return false;
}

bool operator>=(Card a, Card b)
{
    return false;
}

std::ostream& operator<<(std::ostream& os, Card c)
{
    return os << c.get_suit() << c.get_rank();
}

std::istream& operator>>(std::istream& is, Rank& r)
{
    char c;
    is >> c;
    
    switch(c)
    {
        case 'A':
            r = Ace;
            break;
        case '2':
            r = Two;
            break;
        case '3':
            r = Three;
            break;
        case '4':
            r = Four;
            break;
        case '5':
            r = Five;
            break;
        case '6':
            r = Six;
            break;
        case '7':
            r = Seven;
            break;
        case '8':
            r = Eight;
            break;
        case '9':
            r = Nine;
            break;
        case 'T':
            r = Ten;
            break;
        case 'J':
            r = Jack;
            break;
        case 'Q':
            r = Queen;
            break;
        case 'K':
            r = King;
            break;
            
        default:
            std::cout <<"Invalid Input" << std::endl;
            is.setstate(std::ios::failbit);
            break;
    }
    return is;
}

std::istream& operator>>(std::istream& is, Suit& s)
{
    char c;
    is >> c;
    
    switch (c) {
        case 'S':
            s = Spades;
            break;
            
        case 'C':
            s = Clubs;
            break;
            
        case 'D':
            s = Diamonds;
            break;
        case 'H':
            s = Hearts;
            break;
            
        default:
            std::cout <<"Invalid Input" << std::endl;
            is.setstate(std::ios::failbit);
            break;
    }
    
    return is;
}

std::istream& operator>>(std::istream& is, Card& c)
{
    is >> c.m_suit >> c.m_rank;
    return is;
}
