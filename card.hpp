

#pragma once
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <cassert>

enum Suit{
    Spades = 1,
    Clubs,
    Diamonds,
    Hearts,
};

enum Rank{
    Ace = 1,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
};

class Card{

public:
    Card() = default;
    
    Card(Rank r, Suit s); //construct a card with a rank and suit
    
    /*
     Wide Contract:
        throws assertion fail message for invalid inputs
     */
    Card(int s, int r)
    : m_rank(static_cast<Rank>(r)), m_suit(static_cast<Suit>(s))
    {
        assert(s < 4);
        assert(r < 13);
    }
    
    
    Rank get_rank() const
    {
        return m_rank;
    }
    
    Suit get_suit() const
    {
        return m_suit;
    }
    
    friend std::istream& operator>>(std::istream& is, Card& c);
    
private:
    Rank m_rank;
    Suit m_suit;
};

//operator overloading
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);

std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);
std::ostream& operator<<(std::ostream& os, Card c);

std::istream& operator>>(std::istream& is, Rank& r);
std::istream& operator>>(std::istream& is, Suit& s);


