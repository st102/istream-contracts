
#include <iostream>
#include "card.hpp"

int main(int argc, const char * argv[]) {
    
    /*
     Suit: 1-4
     Rank: 1-13
     */
    Card card(5, 1);
    
    Card card1;
    std::cout << "Enter the card in following order:\nSuit Rank \n";
    std::cout <<"Enter a card: ";
    std::cin >> card1;
    
    return 0;
}
